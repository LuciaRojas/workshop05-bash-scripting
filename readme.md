## Bash scripting

# Auntenticacion sin contraseña

Se inicia levantando la maquina virtual 
![coamdo tree](01.jpg "comando tree")

Luego digitamos los siguientes comandos ` eval $(ssh-agent) ssh-add .vagrant/machines/default/virtualbox/private_key` y este   `ssh -p 2222 vagrant@localhost` para entrar a `vagrant@buster`
![coamdo tree](02.jpg "comando tree")

Luego con el comando `vim .bashrc` entramos al documento
![coamdo tree](03.jpg "comando tree")
en este documento agregaremos al fina `echo "Hola $USER"`
![coamdo tree](04.jpg "comando tree")
para ejecutarlo, primero se tiene que forzar y con eso lo hacemos con el siguiente comando sin desconectarse `source .bashrc` y nos tiene que mostrar lo siquiente
![coamdo tree](05.jpg "comando tree")

Ahora vamos agregar otro usuario al sistema con el siguiente comando `sudo adduser mizaq`

![coamdo tree](06.jpg "comando tree")

la cual pide una contraseña la cual sera `secrete` y rellenaremos solo el campo de full name 

![coamdo tree](08.jpg "comando tree")

Ahora para conectarle al nuevo usuario se hace lo siguiente `su mizaq` y luego se le pidira la clave
![coamdo tree](09.jpg "comando tree")

Ahora a continuacion se configurara  el `ll` para eso se necesita entrar al archivo `vim .bashrc`


Ahora estando  en el usario de uno, descomentamos el ll 
![coamdo tree](10.jpg "comando tree")

Y vamos a personalizar un mensaje 
![coamdo tree](11.jpg "comando tree")

volvemos hacer lo mismo que `source .bashrc` y asi podremos ver el mensaje 
![coamdo tree](12.jpg "comando tree")

Saldremos hasta quedar en la maquina de uno para crear una nueva llave para el usuario 

![coamdo tree](13.jpg "comando tree")

A la llave se le colocara el siguiente nombre y ademas por ser solo una llave de prueba no se le colocara clave 
![coamdo tree](14.jpg "comando tree")
con el comando ls -la se pueden ver la llave la cual creara dos archivos
![coamdo tree](15.jpg "comando tree")

Ahora tenemos que hacer que vagrant reconozca a la llave que se acaba de egenerar, para eso se crea una nueva carpte y entramos en ella  

![coamdo tree](16.jpg "comando tree")

Ledamos permisos
![coamdo tree](17.jpg "comando tree")

En ese mismo lugar se crea un archivo el cual se llamara `touch authorized_keys`

Se copia la cadena y se pega en el 
`ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+3L6Hl8u+UfPC89Ca26KSxYKzdBd1kFO1Gwo6ro62bAXEQs4rEbBMKXQvXvy4aBxSNtnH3LbqsH2jtGIxzMZBu7WTeYEtJ4Y8bIfdhxwYbC8+LhZo18qBDG9IWQAkxFWCpCXGIoytNbG/9YzyDtUMaiG4+GfCgNrBKPV0n7WVCEPg/lZx86lJRj3iYpVLSIIZ3fMuP99sCw3icAbGM1GleCxHthkxy6+p4VBevDjyoSvOMCXWmF4TGFXeDOzP/V/CTt3XoXO3PfzK2WtF9hLhlYsStvnp6GFOhcVuT2HDMqyXx9DkMhmwBzZBCgXrtLpRzlB7ghkzUj0CSf19VvZd vagrant@buster`

![coamdo tree](18.jpg "comando tree")
y se pega en el otr archivo

![coamdo tree](19.jpg "comando tree")

#  scripting Bash
Se va a editar el siguiente documento el de arranque 
![coamdo tree](20.jpg "comando tree")
![coamdo tree](21.jpg "comando tree")

Se crea un nuevo archivo y luego se pasa a editarlo
![coamdo tree](22.jpg "comando tree")
en el archivo escribimos lo siguiente 
![coamdo tree](23.jpg "comando tree")
Ahora se tienen quedar persmios `+x` y para que este se pueda utilizar 
![coamdo tree](24.jpg "comando tree")
Ahora para ejecutarlo hacemos lo siguiente
![coamdo tree](25.jpg "comando tree")

Se crea una carpeta nuevo y luego se movera el archivo que creamos anterior menta a esa carpeta
![coamdo tree](26.jpg "comando tree")

Se cambia a quien pertenece con la siguiente linea de comando para que estos archivos pertenezcan a root
![coamdo tree](27.jpg "comando tree")

Ahora utilizaremos ese archivo con algun usuario, asi que entrmos con el usuario que tenemos y ejecutamos ese archivo
![coamdo tree](28.jpg "comando tree")
Ahora a continuacion vamos a crear un elance simbolico y ademas  se va hacer la prueba, ahora tenemos un comando saludar, asi que cada vez que se necesita solo utilizamos el comando saludar, sin la necesidad de rcordar donde esta ese programa 

![coamdo tree](29.jpg "comando tree")

Se va a crear un script en una maquina afintriona 

![coamdo tree](30.jpg "comando tree")

Se va a crear un archivo
![coamdo tree](33.jpg "comando tree")
y se abre con visual code
![coamdo tree](32.jpg "comando tree")
Donde creamos el script con l siguiente informacion
![coamdo tree](34.jpg "comando tree")
Ahora a ese script le vamos a dar permisos
![coamdo tree](35.jpg "comando tree")

A continuacion comprobamos que el script corra correctamente
![coamdo tree](36.jpg "comando tree")
![coamdo tree](37.jpg "comando tree")


Ahora se ve hacer otro de la autimozacion de los backups de la base de datos, para eso primero descargamos el sql que el profesor nos dejo en el campus virtual y los guardaremos en la carpeta scrip que tenemos en buster
![coamdo tree](38.jpg "comando tree")

Luego de eso vamos a copiar los archivos a vagrant@buster, luego se va a crear una base de datos y un usuario, se conecta como super administrador a la base de datos de mysql la cual ya anteriormente se habia instalado con el comando `sudo mysql`


Se crea la base de datos que se va usar `Create database northwind`, luego se crea el usuario `create user laravel@'% indentifed by 'secret'`

se agarantizan los previlegios `grant all privileges on northwind.* to laravel@%`

Se refrescan los prelivi `flush prileges`

luego de que termine de refrescar, se desconecta de la base de datos y se velve a conectar si sudo 

medinate el siguiente comando `mysql -u laravel -p`

`mysql northwind < northwind.sql -u laravel -p`

luego vemos la base de datos northwind se pueden ver las tablas, pero como no tiene datos no se ve, pero tenemos un backup de datos el cual lo vamos a ejecttar 
`mysql northwind < northwind-data.sql -u laravel -p` y ahora si podremos ver los datos

Ahora vamos hacer un script para proteger esa base de datos

se va a trabajar en la maquina afintriona 
se crea un nuevo archivo `touch backup.sh` luego se edita en code `code backup.sh`
![coamdo tree](39.jpg "comando tree")
![coamdo tree](40.jpg "comando tree")
![coamdo tree](41.jpg "comando tree")

El archivo que creamos se va a mover al `vagrant@buster`para eso se tiene que crear una nueva carpeta `mkdir northwind.isw811.xyz` y se dentra a esa carpeta 

luego en la maquina afintriona se va a mover ese scrip `scp backup.sh vagrant@buster-/backups/northiwind.isw811.xyz` ya en el vagrant@buster se le dara el permiso de ejecucion 
 y luego se ejectua el script

 Ahora lo que sigue es automatizar la ejecucion del script 

 Primero se debe de saber la ruta y conocer el nombre del script, luego para utilizar el cron
 se utiliza el siguiente comando `crontab -e`

 ![coamdo tree](42.jpg "comando tree")

en el archivo creado se digita lo siguiente 
![coamdo tree](43.jpg "comando tree")

luego de eso se extrae el backup creado 
